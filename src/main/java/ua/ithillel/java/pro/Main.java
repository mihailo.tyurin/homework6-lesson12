package ua.ithillel.java.pro;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        MyArrayList <String> list = new MyArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        list.add("six");
        list.add("seven");
        list.add("eight");
        list.add("nine");
        list.add("ten");
        System.out.println(list.size());
        list.remove(5);
        System.out.println(list.size());

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

}
