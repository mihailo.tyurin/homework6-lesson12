package ua.ithillel.java.pro;

import java.util.Arrays;
import java.util.List;

public class MyArrayList<T> implements MyList<T> {

    private Object[] arr = {};

    @Override
    public void add(T elem) {
        arr = Arrays.copyOf(arr, arr.length + 1);
        arr[arr.length - 1] = elem;
    }

    @Override
    public void insert(T elem, int index) {
        arr = Arrays.copyOf(arr, arr.length + 1);
        for (int i = arr.length - 1; i >= index; i--) {
            arr[i] = arr[i - 1];
        }

    }

    @Override
    public void remove(int index) {
        for (int i = 0; i < arr.length - 1; i++) {
            if (i < index-1) {
                continue;
            } else {
                arr[i] = arr[i + 1];
            }
        }
        arr = Arrays.copyOf(arr, arr.length - 1);
    }

    @Override
    public void remove(T value) {
        int sum = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i].equals(value) & sum < 1) {
                arr[i] = arr[i + 1];
                sum++;
            } else {
                arr[i] = arr[i + 1];
            }
        }
        arr = Arrays.copyOf(arr, arr.length - 1);
    }

    @Override
    public int indexOf(T value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(value)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(T value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(value)) return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(MyList<T> values) {
        for (int i = 0; i < values.size(); i++) {
            for (int j = 0; j < arr.length; j++) {
                if (values.equals(arr[j])) {
                    continue;
                } else return false;
            }
        }
        return true;
    }

    @Override
    public T get(int index) {
        return (T) arr[index];
    }

    @Override
    public void set(int index, T value) {
        arr[index] = value;
    }

    @Override
    public int size() {
        return arr.length;
    }

}
